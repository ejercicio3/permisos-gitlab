# informe de permisos

###Introducción 
El presente informe busca resumir las diferencias entre los diferentes roles dentro del gitlab
###Objetivo general
Observar las diferencias entre roles dentro de Gitlab
###¿Qué es Gitlab?
Gitlab es un sistema de repositorios que nos permite tener un histórico de los cambios realizados a diferentes archivos
###información de a que repositorio
El repositorio en el que trabajamos para este proyecto se encuentra en el siguiente link
[Respositorio](https://gitlab.com/repositorios-capacitacio/mi-primer-proyecto)


##Análisis

###ROL: Invitado


![Invitado](https://i.ibb.co/mN5ytSw/image.png)

####tabla comparativa con las actividades




Crear issues | Lista de issues | Editar los issues creados por mi y por otra persona | agrear etiquetas  en issues| crear etiquetas | agregar miembros| cambiar de rol a los miembros | asignar issues| cerrar issues que yo cree | cerrar issues que otros han creado
------------ | -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------
Si | Si| No| No| No| No| No| No| Si| No

- [x] Crear issues 
- [x] Lista de issues
- [ ] Editar los issues creados por mi y por otra persona
- [ ] agrear etiquetas  en issues 
 
- [ ] crear etiquetas
- [ ] agregar miembros
- [ ] cambiar de rol a los miembros
- [ ] asignar issues
- [x] cerrar issues que yo cree 
- [ ] cerrar issues que otros han creado

###ROL: Reporter


![Reporter](https://i.ibb.co/xJZ4GMX/image.png)

####tabla comparativa con las actividades

Crear issues | Lista de issues | Editar los issues creados por mi y por otra persona | agrear etiquetas  en issues| crear etiquetas | agregar miembros| cambiar de rol a los miembros | asignar issues| cerrar issues que yo cree | cerrar issues que otros han creado
------------ | -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------
Si | Si| No| Si| Si| No| No| No| Si| Si

- [x] Crear issues 
- [x] Lista de issues
- [ ] Editar los issues creados por mi y por otra persona
- [x] agrear etiquetas  en issues 
 
- [X] crear etiquetas
- [ ] agregar miembros
- [ ] cambiar de rol a los miembros
- [ ] asignar issues
- [x] cerrar issues que yo cree 
- [X] cerrar issues que otros han creado

###ROL: Developer


![Reporter](https://i.ibb.co/5BHq4Nh/image.png)

####tabla comparativa con las actividades

Crear issues | Lista de issues | Editar los issues creados por mi y por otra persona | agrear etiquetas  en issues| crear etiquetas | agregar miembros| cambiar de rol a los miembros | asignar issues| cerrar issues que yo cree | cerrar issues que otros han creado
------------ | -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------
Si | Si| Si| Si| Si| No| No| Si| Si| Si

- [x] Crear issues 
- [x] Lista de issues
- [X] Editar los issues creados por mi y por otra persona
- [x] agrear etiquetas  en issues 
 
- [X] crear etiquetas
- [ ] agregar miembros
- [ ] cambiar de rol a los miembros
- [x] asignar issues
- [x] cerrar issues que yo cree 
- [X] cerrar issues que otros han creado

###ROL: Maintainer



![ImagenReporter](https://i.ibb.co/HDMwq7z/image.png)

####tabla comparativa con las actividades

Crear issues | Lista de issues | Editar los issues creados por mi y por otra persona | agrear etiquetas  en issues| crear etiquetas | agregar miembros| cambiar de rol a los miembros | asignar issues| cerrar issues que yo cree | cerrar issues que otros han creado
------------ | -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------| -------------
Si | Si| Si| Si| Si| Si| Si| Si| Si| Si

- [x] Crear issues 
- [x] Lista de issues
- [X] Editar los issues creados por mi y por otra persona
- [x] agrear etiquetas  en issues 
- [X] crear etiquetas
- [X] agregar miembros
- [x] cambiar de rol a los miembros
- [x] asignar issues
- [x] cerrar issues que yo cree 
- [X] cerrar issues que otros han creado


##conclusiones

- Es importante identificar el rol asignado a cada miembro puesto que un rol con demasiados permisos 
puede permitir la fuga de información o perdida del repositorio.

